package com.ramattec.myfoodr.ui.home

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.ViewModel
import com.ramattec.myfoodr.ui.util.AlertReceiver
import com.ramattec.myfoodr.ui.util.Constants
import com.ramattec.myfoodr.ui.util.DateUtils
import com.ramattec.myfoodr.ui.util.MyPreferences
import java.text.DateFormat
import java.util.*

class HomeViewModel(context: Context) : ViewModel() {

    private var alarmMgr: AlarmManager? = null
    private lateinit var alarmIntent: PendingIntent
    val preferences: MyPreferences = MyPreferences(context)

    fun saveHour(hour: String) {
        preferences.save(Constants.Preferences.PREFS_HOUR, hour)
        Log.d(Constants.LogLevel.LOG_HOME_VIEWMODEL,
            "valor salvo de hora ${preferences.getValue(Constants.Preferences.PREFS_HOUR)}")
    }

    fun saveInterval(interval: String){
        preferences.save(Constants.Preferences.PREFS_INTERVAL, interval)
        Log.d(Constants.LogLevel.LOG_HOME_VIEWMODEL,
            "valor salvo de intervalo ${preferences.getValue(Constants.Preferences.PREFS_INTERVAL)}")
    }

    fun setAlarm(context: Context){
        alarmMgr = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmIntent = Intent(context, AlertReceiver::class.java).let {
            PendingIntent.getBroadcast(context, 0, it, 0)
        }

        val calendar: Calendar = Calendar.getInstance().apply {
            timeInMillis = System.currentTimeMillis()
            DateUtils.convertStringInMilliseconds(preferences.getValue(Constants.Preferences.PREFS_HOUR))
        }

        alarmMgr?.setRepeating(
            AlarmManager.RTC_WAKEUP,
            calendar.timeInMillis,
            DateUtils.convertStringInMilliseconds(preferences.getValue(Constants.Preferences.PREFS_INTERVAL)),
            alarmIntent
        )

        Toast.makeText(context,
            "Alarm set for : $DateUtils.convertStringInMilliseconds(preferences.getValue(Constants.Preferences.PREFS_HOUR)",
            Toast.LENGTH_LONG).show()
    }

    fun updateTimeText(time: Calendar, text_first_alarm: TextView) {
        var text = "Alarm set for: "
        text += DateFormat.getTimeInstance(DateFormat.SHORT).format(time.time)
        text_first_alarm.text = text
    }

}
