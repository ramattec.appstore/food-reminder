package com.ramattec.myfoodr.ui.application

import android.app.Application
import com.ramattec.myfoodr.ui.injection.appModule
import com.ramattec.myfoodr.ui.util.MyPreferences
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin


class FoodReminderApplication : Application(){
    override fun onCreate() {
        super.onCreate()
        //startKoin
        startKoin {
            androidLogger()
            androidContext(this@FoodReminderApplication)
            modules(appModule)
        }
    }
}