package com.ramattec.myfoodr.ui.util

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    fun convertStringInMilliseconds(value: String): Long {
        val formatter: SimpleDateFormat = SimpleDateFormat("HH:mm")
        val date = formatter.parse(value)
        return date.time
    }

    fun convertMillisecondsInDate(value: Long): String{
        val sdf = SimpleDateFormat("HH:mm")
        val result = Date(value)
        return sdf.format(result)
    }

    fun getNow(): String{
        val calendar = Calendar.getInstance()
        val sdf = SimpleDateFormat("HH:mm")
        return sdf.format(calendar.time)
    }

    fun showNextTime(interval: String, last: String): String{
        val sum = convertStringInMilliseconds(last) + convertStringInMilliseconds(interval)
        return convertMillisecondsInDate(sum)
    }

}