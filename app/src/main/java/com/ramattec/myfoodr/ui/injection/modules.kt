package com.ramattec.myfoodr.ui.injection

import com.ramattec.myfoodr.ui.home.HomeViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module


val appModule = module {
    viewModel { HomeViewModel(get()) }
}
