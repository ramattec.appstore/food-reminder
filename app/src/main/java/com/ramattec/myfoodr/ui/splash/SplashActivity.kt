package com.ramattec.myfoodr.ui.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.ramattec.myfoodr.R
import com.ramattec.myfoodr.ui.home.HomeActivity

class SplashActivity : AppCompatActivity() {
    private val SPLASH_DELAY: Long = 7000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            startActivity(Intent(this, HomeActivity::class.java))
            finish()
        }, SPLASH_DELAY)
    }
}
