package com.ramattec.myfoodr.ui.home

import android.content.Context
import com.ramattec.myfoodr.ui.util.Constants
import com.ramattec.myfoodr.ui.util.MyPreferences
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.mock

class HomeViewModelTest {

    val context = mock(Context::class.java)
    val preferences: MyPreferences = mock(MyPreferences(context)::class.java)
    val homeViewModel: HomeViewModel = mock(HomeViewModel(context)::class.java)

    @Before
    fun setUp(){
    }

    @Test
    fun saveHour() {
        val hour = "7:00"
        homeViewModel.saveHour(hour)
        assertEquals("6:00", preferences.getValue(Constants.Preferences.PREFS_HOUR))
    }

    @Test
    fun saveInterval() {
        val interval = "3:30"
        homeViewModel.saveInterval(interval)
        assertEquals("3:30", preferences.getValue(Constants.Preferences.PREFS_INTERVAL))
    }

    @Test
    fun getPrefetences() {

    }

    @Test
    fun updateTimeText() {
    }



    @Test
    fun setAlarm() {
    }
}