package com.ramattec.myfoodr.ui.util

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.ramattec.myfoodr.R
import com.ramattec.myfoodr.ui.splash.SplashActivity

class NotificationBuilder{

    companion object{
        const val CHANNEL_ID = "01"

        fun getNotificationBuilder(context: Context): NotificationCompat.Builder? {

            val intent = Intent(context, SplashActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }

            val pendingIntent = PendingIntent.getActivity(context, 0, intent, 0)
            var builder = NotificationCompat.Builder(context!!,
                CHANNEL_ID
            )
                .setSmallIcon(R.drawable.notification_icon_background)
                .setContentTitle(context.getString(R.string.title_notification_example))
                .setContentText(context.getString(R.string.short_message_notification_example))
                .setStyle(
                    NotificationCompat.BigTextStyle()
                        .bigText(context.getString(R.string.long_message_notification_example)))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setCategory(NotificationCompat.CATEGORY_REMINDER)
                .addAction(R.drawable.abc_ic_commit_search_api_mtrl_alpha, "OK", pendingIntent)
                .addAction(R.drawable.abc_ic_arrow_drop_right_black_24dp, "Cancel", pendingIntent)

            return builder
        }

        fun createNotificationChannel(context: Context){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                val name = "Canal de notificacao"
                val descriptionText = "Descricao do canal"
                val importance = NotificationManager.IMPORTANCE_DEFAULT
                val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                    description = descriptionText
                }
                // Register the channel with the system
                val notificationManager: NotificationManager =
                    context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

                notificationManager.createNotificationChannel(channel)
            }
        }

        class NotificationHelper{
            companion object{
                fun sendNotification(builder: NotificationCompat.Builder, context: Context) {
                    createNotificationChannel(context)
                    val preferences = MyPreferences(context)

                    preferences.save(Constants.Preferences.PREFS_LAST_REMINDER, DateUtils.getNow())

                    with(NotificationManagerCompat.from(context)){
                        //notificationId is Unique
                        notify(1, builder.build())
                    }
                }
            }
        }

    }
}