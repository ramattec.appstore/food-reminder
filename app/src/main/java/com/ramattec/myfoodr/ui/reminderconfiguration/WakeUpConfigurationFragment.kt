package com.ramattec.myfoodr.ui.reminderconfiguration


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.ramattec.myfoodr.R
import com.ramattec.myfoodr.ui.home.HomeFragment
import com.ramattec.myfoodr.ui.home.HomeViewModel
import kotlinx.android.synthetic.main.fragment_wake_up_configuration.*
import org.koin.android.viewmodel.ext.android.viewModel

class WakeUpConfigurationFragment : Fragment() {

    companion object {
        fun newInstance() = WakeUpConfigurationFragment()
    }

    val homeViewModel: HomeViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_wake_up_configuration, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button_ok_wakeup.setOnClickListener {
            homeViewModel.saveHour(edit_hour_wake_up.text.toString())
            openIntervalFragment()
        }
    }

    private fun openIntervalFragment(){
        activity?.supportFragmentManager!!.beginTransaction()
            .replace(R.id.container, IntervalFragment.newInstance())
            .commitNow()
    }


}
