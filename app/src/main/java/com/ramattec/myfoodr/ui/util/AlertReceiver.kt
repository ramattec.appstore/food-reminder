package com.ramattec.myfoodr.ui.util

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class AlertReceiver : BroadcastReceiver(){

    override fun onReceive(context: Context?, intent: Intent?) {
        val builder = context?.let {
            NotificationBuilder.getNotificationBuilder(
                it
            )
        }
        builder?.let {
            NotificationBuilder.Companion.NotificationHelper.sendNotification(
                it,
                context
            )
        }
    }
}