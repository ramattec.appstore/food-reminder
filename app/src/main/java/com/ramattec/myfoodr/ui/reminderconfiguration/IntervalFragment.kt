package com.ramattec.myfoodr.ui.reminderconfiguration


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ramattec.myfoodr.R
import kotlinx.android.synthetic.main.fragment_interval.*
import android.widget.ArrayAdapter
import com.ramattec.myfoodr.ui.home.HomeFragment
import com.ramattec.myfoodr.ui.home.HomeViewModel
import org.koin.android.viewmodel.ext.android.viewModel


class IntervalFragment : Fragment() {

    companion object{
        fun newInstance() = IntervalFragment()
    }

    val homeViewModel: HomeViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_interval, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setSpinner()

        button_ok_interval.setOnClickListener {
            getInterval()
            openHomeFragment()
        }
    }

    private fun getInterval(){
        val position = sppiner_interval.selectedItemPosition
        homeViewModel.saveInterval(sppiner_interval.getItemAtPosition(position).toString())
    }

    private fun openHomeFragment(){
        activity?.supportFragmentManager!!.beginTransaction()
            .replace(R.id.container, HomeFragment.newInstance())
            .commitNow()
    }

    private fun setSpinner(){
        val adapter = ArrayAdapter.createFromResource(
            context,
            R.array.time_array, android.R.layout.simple_spinner_item
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        sppiner_interval.adapter = adapter
    }


}
