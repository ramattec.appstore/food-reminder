package com.ramattec.myfoodr.ui.util

import android.content.Context
import android.content.SharedPreferences
import com.ramattec.myfoodr.ui.util.Constants.Preferences.Companion.PREFS_FILENAME

class MyPreferences(context: Context) {
    val sharedPreferences: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)

    fun save(KEY_NAME: String, value: String){
        val editor: SharedPreferences.Editor = sharedPreferences.edit()

        editor.putString(KEY_NAME, value)
        editor.commit()
    }

    fun getValue(KEY_NAME: String): String{
        return sharedPreferences.getString(KEY_NAME, "")
    }

    fun clearPreferences(){
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.clear()
        editor.commit()
    }

    fun removeSpecificData(KEY_NAME: String){
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.remove(KEY_NAME)
        editor.commit()
    }
}