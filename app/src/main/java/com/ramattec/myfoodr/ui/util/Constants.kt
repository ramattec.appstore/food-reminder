package com.ramattec.myfoodr.ui.util

object Constants {
    class Preferences{
        companion object{
            const val PREFS_FILENAME = "con.ramattec.myfoodreminder.FoodReminder"
            const val PREFS_HOUR = "hour"
            const val PREFS_INTERVAL = "interval"
            const val PREFS_LAST_REMINDER = "last_reminder"
        }
    }

    class LogLevel{
        companion object{
            const val LOG_HOME_VIEWMODEL = "HOME_VIEWMODEL"
            const val NOTIFICATION_BUILDER = "NOTIFICATION_BUILDER"
        }
    }
}


