package com.ramattec.myfoodr.ui.home

import android.app.TimePickerDialog
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TimePicker
import androidx.lifecycle.ViewModelProviders
import com.ramattec.myfoodr.R
import com.ramattec.myfoodr.ui.reminderconfiguration.WakeUpConfigurationFragment
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*

class HomeActivity : AppCompatActivity(), TimePickerDialog.OnTimeSetListener {

    val homeViewModel: HomeViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_activity)

        if (savedInstanceState == null) {
            openFragment()
        }
    }

    private fun openFragment(){
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, WakeUpConfigurationFragment.newInstance())
            .commitNow()
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        val c = Calendar.getInstance()
        c.set(Calendar.HOUR_OF_DAY, hourOfDay)
        c.set(Calendar.MINUTE, minute)
        c.set(Calendar.SECOND, 0)

    }

}
