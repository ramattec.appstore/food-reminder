package com.ramattec.myfoodr.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.ramattec.myfoodr.R
import com.ramattec.myfoodr.ui.util.Constants
import com.ramattec.myfoodr.ui.util.DateUtils
import com.ramattec.myfoodr.ui.util.MyPreferences
import com.ramattec.myfoodr.ui.util.NotificationBuilder
import kotlinx.android.synthetic.main.home_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {

    companion object {
        fun newInstance() = HomeFragment()
    }
    val homeViewModel: HomeViewModel by viewModel()
    lateinit var preferences: MyPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.home_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        preferences = MyPreferences(context!!)
//        configNotification()
        homeViewModel.setAlarm(context!!)
        setPreviewNextAlarm()
    }

    fun setPreviewNextAlarm(){
        if (preferences.getValue(Constants.Preferences.PREFS_LAST_REMINDER) != ""){
            val next = DateUtils.showNextTime(preferences.getValue(Constants.Preferences.PREFS_INTERVAL),
                preferences.getValue(Constants.Preferences.PREFS_LAST_REMINDER))
            textView.text = "Next Alarm At: $next"
        } else {
            textView.text = "Next Alarm At: $preferences.getValue(Constants.Preferences.PREFS_HOUR)"
        }
    }


    fun configNotification(){
        val builder = context?.let { NotificationBuilder.getNotificationBuilder(it) }
        builder?.let { NotificationBuilder.Companion.NotificationHelper.sendNotification(it, activity!!.applicationContext) }
    }

}
